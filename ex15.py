# Exercice 15: Reverse word order

# Write a program that asks the user to enter a string of multiple words : 
# Print back to the user the same string, except with the words in backwards
# order

def ask_string():

    user_string = str(input("Please enter a phrase of your choice \n"))
    return user_string

def reverse_string(string):
    new_string = string.split()
    reversed_string = new_string[::-1]
    return reversed_string

test_string = ask_string()

print(reverse_string(test_string))
