# Exercice 16 : Random Password Generator

# Create a random password generator

# Extra: Ask the user how strong they want their password to be
# if they want an easy password, choose a password from a list of words

import time
import random
import string

def generate_password():

    s = string.ascii_letters + string.digits + "&(§!)$£%.;?><"

    passwd = ""

    for char in range(1, 19):

        passwd = passwd + random.choice(s)

    return passwd

def generate_easy_password():

    choices = ["123456", "qwerty", "baseball", "football", "monkey", "leitmen"]

    easy_pwd = random.choice(choices)

    return easy_pwd

def main():

    name = input("Hey, What's your name ? \n")
    generated = False
    time.sleep(1)

    print("Hey %s, we are going to generate a password for you \n" % (name))

    time.sleep(1)

    pwd_type = input("If you want an easy password, type 'yes' otherwise type 'no' \n")

    time.sleep(1)

    print("Generating your password ...")

    time.sleep(2)

    if pwd_type == "yes":
        print(generate_easy_password())
    elif pwd_type == "no":
        print(generate_password())
    else: 
        print("Sorry, you have to type yes for an easy password an no for a\
 strong one")


if __name__ == "__main__":
    main()
