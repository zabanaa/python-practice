# Exercice 5: List overlap

# Create two lists and return a third one containing only the element that are
# common to the two lists, it has to work on two lists of different sizes
# Extra : Generate a random list
# Extra 2: Do it in a oneliner

import random
import time

rand_list_one = random.sample(range(90), 9)
rand_list_two = random.sample(range(90), 12)

list_one = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
list_two = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

def main():

    common_list = []

    for number in list_two:

        if number in list_one:

            common_list.append(number)
    print("Here are the numbers that are common to both lists ... ")
    time.sleep(1)
    print(common_list)

def extra():

    common_random_list = []

    for number in rand_list_two:

        if number in rand_list_one:

           common_random_list.append(number)

    print("Here are the numbers that are common to both lists ... ")
    time.sleep(1)

    if len(common_random_list) == 0:
        print("Sorry there are no matches, try again")
    else:
        print(common_random_list)


if __name__ == "__main__":
    extra()
