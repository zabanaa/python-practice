# Exercise 23: File Overlap

# Given two txt files find the numbers that are overlapping 


import time

def main(file1, file2):
    prime_numbers = open(file1, "r").readlines()
    happy_numbers = open(file2, "r").readlines()
    primes = []
    happys = []


    for prime_num in prime_numbers:
        prime = prime_num.replace('\n', '')
        primes.append(int(prime))

    for happy_num in happy_numbers:
        happy = happy_num.replace("\n", "")
        happys.append(int(happy))

    overlapping = [num for num in primes if num in happys]

    print("These are the overlapping numbers")
    time.sleep(1)
    print(overlapping)


if __name__ == "__main__":
    main("file-one.txt", "file-two.txt")
