# Exercice 12 : List Ends

# Write a program that takes a list of numbers and makes a new list of only the
# first and last items

def retrieve_first_and_last(lst):

    new_list = []
    first_item = lst[0]
    last_item = lst[-1]

    new_list.append(first_item)
    new_list.append(last_item)

    return new_list

list_a  = [23, 43, 33, 67, 78, 987, 67, 837, 282828, 98]
list_b  = [65, 43, 33, 67, 78, 987, 67, 837, 282828, 77]

new_list_a = retrieve_first_and_last(list_a)
new_list_b = retrieve_first_and_last(list_b)

print(new_list_a)
print(new_list_b)
