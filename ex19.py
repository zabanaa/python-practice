# Exercice 19: Decode A Web Page 2

# Using the requests and BeautifulSoup libraries, print to the screen the full
# text of the article on this website

# The tasks is to retrieve the article on the vanity fair website
# Extra, print all the titles from pc advisor, multiple pages without going to
# the browser
import requests
from bs4 import BeautifulSoup
import time

url = \
"http://www.vanityfair.com/style/society/2014/06/monica-lewinsky-humiliation-culture"

def retrieve_article(article_url):
    req = requests.get(article_url)
    response = req.text
    soup = BeautifulSoup(response, "html.parser")
    paragraphs = soup.find_all("section", {"class":"content-section"})

    for paragraph in paragraphs[1::]:
        print(paragraph.text)


retrieve_article(url)
