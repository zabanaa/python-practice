# Exercice 3: List less than ten

# Take a list, and list all numbers that are less than 5

# Extras:
# Make a new list and add the numbers to it, then return that new list
# Write this in python
# Ask the user for a number, then return a list that contains numbers from the
# original list that are less than the number entered by the user.
import time

list = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

def less_than_5():

    for number in list:

        if number < 5:
            print(number)

def extra():
    new_list = []

    for number in list:
        if number < 5:
            new_list.append(number)
    print(new_list)


def extra_2():
    user_number = int(input("Give me a number \n"))
    new_list = []
    for number in list:
        if number < user_number:
            new_list.append(number)
    print("There are " + str(len(new_list)) + " numbers less than the one you \
    entered in that list.")

    time.sleep(1)
    print("Here they are ...")
    time.sleep(1)
    print(new_list)

if __name__ == "__main__":
    less_than_5 = [number for number in list if number < 5]
    print(less_than_5)
