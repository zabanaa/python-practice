# Exercise 24: Draw a game board

# Ask the user the size of the board and display it on the screen using print
# statements

def main(size):

    for i in range(1, (size + 1)):

        print(" --- " * size)

        print("|    " * (size + 1))
    
    print(" --- " * size)








if __name__ == "__main__":
    main(3)
