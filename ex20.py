# Exercice 20: List Search

# Write a function that takes an ordered list of numbers and another one, the
# function decides wheter or not the given number is in the list then prints a
# boolean value
import random

def list_search(numlist, num):

    for number in numlist:

        if number == num:
            print("Hey ! We've got a match for number %s" % (number))
        else:
            print("Number %s does not match your number" % (number))

random_list = sorted(random.sample(range(1, 201), 14))
random_int = random.randint(1, 200)

list_search(random_list, random_int)
