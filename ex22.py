# Exercice 22

# Given a list of names inside a txt file, read it and print out the number of
# occurrences for each name.

def main(filename):

    read_file = open(filename, 'r')
    names = read_file.readlines()
    list_of_names = {} 

    for name in names:

        if name in list_of_names:
            list_of_names[name] += 1
        else:
            list_of_names[name] = 1

if __name__ == "__main__":
    main("names.txt")
