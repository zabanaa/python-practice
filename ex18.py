# Exercice 18: Cows and Bulls

# Randomly Generate 4 numbers
# Ask the user to guess 4 numbers
# For every digit that the user guessed correctly in the correct place,
# increment cow. 
# For every digit that the user guessed in the wrong place, increment bulls
# The game keeps running until the user finds the correct number 
# After the game is over, print to him the total number of bulls and cows

import random
import time

def generate_number():
   random_digits = random.sample(range(1, 10), 4)
   new_num = ""
   for digit in random_digits:
       new_num += str(digit)
   return new_num

def main():
    playing = True
    shots = 4
    random_number = generate_number()
    guessed_digits = []
    print("Welcome to the cows and bulls game")
    print("I will generate a random 4 digit number")
    print("You will have to enter a 4 digit number")
    print("For each digit that you guess correctly at the correct place")
    print("It's a cow")
    print("For each digit that you guess correctly but that is not at the\
 correct place, it's a bull")
    print("To quit the game, just type exit in the prompt")
    print("Good Luck")

    time.sleep(1)

    while playing:

        user_guess = input("Please Enter a 4 digit number it should not start\
 with 0 \n").lower()
        cows = 0
        bells = 0

        if len(user_guess) != 4:

            print("You have to guess a 4 digit number")

        elif user_guess == "exit":
            break
        else:

            shots = shots - 1
            for digit, number  in zip(user_guess, random_number):
                if digit in random_number:

                    if digit == number and digit not in guessed_digits:
                        guessed_digits.append(digit)
                        cows += 1

                    else:
                        bells += 1

            print("Cows Guessed: %s/4 - Turns Left: %s/4 "\
 %(len(guessed_digits), shots))

            if shots == 0:
                print("Game is over, you have no shots left")
                print("Here's the final score")
                time.sleep(1)
                print("Cows: %s, Bells: %s" % (cows, bells))
                print("This is the actual number I came up with")
                time.sleep(1)
                print(random_number)
                break

if __name__ == "__main__":
    main()






