# Exercise 25: Guessing Game Part 2

# The author of the program will have to think of a number between 1 and 100
# then ask the computer to guess the number. After each try, the author has to
# inform the computer whether or not his guess is too low, too high or if it
# guessed the correct number

import time
import random


if __name__ == "__main__":


    random_guess = random.randint(1, 100)
    found = False
    tries = 0

    print("Alright, computer I will think of a number between 1 and 100\
 , your job will be to guess it !")

    time.sleep(1)

    print("Go !")

    time.sleep(1)

    while not found:
        # the computer prints a guess
        print("Here's my guess: %s" % (random_guess))
        check_guess = input("Did I get it ? \n").lower()

        # I tell it to guess lower or higher
        if check_guess == "guess higher":

            print("Ok, I will have to guess higher than my current number")
            random_guess = random.randint(random_guess, 100)
        elif check_guess == "guess lower":
            print("Ok, I will have to guess lower than my current number")
            random_guess = random.randint(1, random_guess)
        elif check_guess == "got it":
            found = True
            if tries > 5:
                print("Hey that was pretty tough, it took me %s tries"\
                      %(tries))
            else:
                print("Only %s tries ?! Make it harder man" %(tries))
        tries += 1


