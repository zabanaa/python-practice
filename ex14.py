# Exercice 14

# Write a function that takes a list and returns all the elements of the first
# minus the duplicates
# Extra write a function that does this with sets instead

def remove_duplicates(lst):

    new_list = []

    for number in lst:

        if number in new_list:

            continue
        else:
            new_list.append(number)

    return new_list

list_a = [1, 1, 2, 3, 4, 4, 4, 87, 80, 956, 956, 97, 65, 354, 234, 342, 234]

def extra(lst):

    new_set = set(lst)
    new_list = list(new_set)
    return new_list
print(extra(list_a))
