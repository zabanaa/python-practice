#Exercice 8: Rock Paper Scissors

# Rules:

# Rock Beats Scissors
# Scissors beats paper
# Paper beats rock

# ask the user how many rounds they want to play
# then keep track of the wins and losses of each player
# at the end of the game, ask the user if they want to quit, 
# display the table

import time
import random 

choices = ["Rock", "Paper", "Scissors"]

def check_winner(p1_score, p2_score, name):

    if p1_score  == p2_score:
        print("%s: %s - Player 2: %s, it's a tie !!" %\
        (name, p1_score , p2_score))

    elif p1_score  > p2_score:
        print("%s: %s - Player 2: %s, Player 1 wins" %\
        (name, p1_score , p2_score))
    else:
        print("%s: %s - Player 2: %s, Player 2 wins" %\
        (name, p1_score , p2_score))

def game():

    print("Hi There, Welcome to Rock Paper Scissors")
    time.sleep(1)
    player_name = input("What's your name ? ... ")
    time.sleep(1)
    turns = int(input("How many rounds do you want to play ? \n"))
    time.sleep(1)
    print("Perfect %s, Let the game begin !!" % (player_name))
    player_one_score = 0
    player_two_score = 0

    while turns > 0:

        player_one_choice = input("Player 1: Your Choice : \n").lower()
        player_two_choice = random.choice(choices).lower()

        if player_one_choice == player_two_choice:

            print("It's a tie, try again")
            turns = turns - 1
            player_one_score = player_one_score
            player_two_score = player_two_score

        elif player_one_choice == "rock":

            if player_two_choice == "scissors":

                print("Rock Beats Scissors, Player one wins")
                player_two_score = player_two_score
                player_one_score = player_one_score + 1

            else:

                print("Paper beats rock, Player two wins")
                player_one_score = player_one_score
                player_two_score = player_two_score + 1

            turns = turns - 1

        elif player_one_choice == "scissors":

            if player_two_choice == "paper":
                print("Scissors Cut Paper, Player one wins")
                player_one_score = player_one_score + 1
                player_two_score = player_two_score
            else:
                print("Rock crushes scissors, Player Two Wins")
                player_two_score = player_two_score + 1
            turns = turns - 1

        elif player_one_choice == "paper":

            if player_two_choice == "Rock".lower():
                print("Paper beats rock, player one wins")
                player_one_score += 1
            else:
                print("Scissors cut paper, player two wins")
                player_two_score += 1
            turns = turns - 1
        else:
            print("You haven't entered 'Rock', 'Paper' or 'Scissors', please\
            try again")
            break

        if turns == 0:
           more_rounds = input("The game is over, there are no more rounds \
           to play, do you want to play again ? (press 'y' for yes and 'n' for\
           no")

           if more_rounds == 'y':
               print("You will play another game shortly ...")
               time.sleep(1)
               game()
           elif more_rounds == 'n':
               print("Thanks for playing, here are the results of the game")
               time.sleep(1)
               check_winner(player_one_score, player_two_score, player_name)
game()
