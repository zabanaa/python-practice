# Exercice 2

# Ask the user for a number
# Depending on whether the number is odd or even, print an appropriate message
# if the number is a multiple of 4, print a different message

# Extra:
# ask the user for a number to check and a number to divide by , if the first
# number is divisible by the second then print it, otherwise print a different
# message
import time

def odd_or_even():
    print("Hey Welcome to my program, you will be asked to choose a number")
    time.sleep(1)
    user_number = int(input("Give my a number: "))
    if user_number % 4 == 0:
        print("your number is divisible by 4")
    elif user_number % 2 == 0:
        print("the number you gave me is even")
    else:
        print("the number you gave me is odd")

def number_check():
    user_num_check = int(input("Give me a number to check "))
    user_num_divide = int(input("Give me a number to divide by "))

    if user_num_check % user_num_divide == 0:
        print("Yes indeed! " + str(user_num_check) + " is divisible by " +
                str(user_num_divide))
    else: 
        print("Sorry mate, " + str(user_num_check) + " is not divisible by " +
                str(user_num_divide))



if __name__ == "__main__":
    number_check()
