# Create a program that asks the user for a number and that returns a list of
# all the divisors for that number
import time

def main():
    user_number = int(input("Choose a number \n"))
    num_range = list(range(1,user_number + 1))
    divisors_list = []

    for number in num_range:

        if user_number % number == 0:
            divisors_list.append(number)
    print("Here's a list of all divisors for " + str(user_number) + "... ")
    time.sleep(1)
    print(divisors_list)






if __name__ == "__main__":
    main()
