# Exercice 6 : List Comprehension

# Take a list and return all even numbers using list comprehension

a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
even_numbers = [number for number in a if (number % 2 == 0)]
print(even_numbers)

def main():
    pass


if __name__ == "__name__":
    main()


