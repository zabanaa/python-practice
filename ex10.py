# List Overlap Comprehension

# Take two lists and write a program that returns a list that contains only the
# elements that are common to both lists 
# Achieve this by using list comprehrension
# Do it using two randomly generated lists

import time
import random

list_a = random.sample(range(1, 201), 14)
list_b = random.sample(range(1, 201), 32)

def main():

    list_c = [number for number in set(list_a) if number in list_b]
    print("Looking for common items ...")
    time.sleep(1)
    if len(list_c):
        print("This is a list of numbers common to both list 1 and 2")
        time.sleep(1)
        print(list_c)

    else:
        print("There are no matches between list a and list b sorry")

if __name__ == "__main__":
    main()
