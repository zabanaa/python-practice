# Exercice 9: Guessing Game

# Generate a random number between 1 and 9 (inclusive)
# Ask the user to guess the number and tell them if they've guessed too high,
# too low or exactly right 

# Extras

# Keep the game going until the user types "exit"
# Keep track of how many tries it took them to guess the number and at the end
# print it out

import random
import time

def main():

    attempts = 0
    number_found = False
    user_name = input("Hi There, What's Your Name ? \n")
    random_number = random.randint(1, 9)

    time.sleep(1)

    print("Ok %s, I will think of a number between 1 and 9\
            and you will have to guess it alright ? ..." % (user_name))

    time.sleep(2)

    print("To quit the game, just type exit")

    time.sleep(2)

    print("Ok, I've chosen a number !")

    time.sleep(2)

    while not number_found:

        user_guess = input("Take a guess ! Remember, it has to be between\
         1 and 9 ! \n")

        if user_guess == "exit":
            print("Quitting the game ...")
            time.sleep(1)
            break

        elif int(user_guess) == random_number:
            print("Wow ! You guessed the number, brilliant !")
            time.sleep(1)
            print("It took you %s attempts to guess the number ! \
                    " % (attempts + 1))
            number_found = True

        elif int(user_guess) > random_number:
            print("Oops, too high ! Guess lower :)")

        else:
            print("Oops, too low ! Guess higher :)")

        attempts += 1

if __name__ == "__main__":
    main()


