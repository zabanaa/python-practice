# Exercice 21: Requests and File Input

# Using the requests and BeautifulSoup Library, print all the article titles to
# a file
# ask the user what they want the file name to be

import requests
from bs4 import BeautifulSoup
import time

def get_article_titles(url, filename):

    new_file = open(filename + ".txt", "a")

    req = requests.get(url)
    response = req.text
    soup = BeautifulSoup(response, "html.parser")
    titles = soup.find_all("h2")

    for title in titles:
        title_text = title.find("a").text
        new_file.write(title_text + "\n")
    new_file.close()

def main():
    i = 1
    filename = input("What do you want the filename to be ? \n")

    while i < 5:

        url = "http://www.pcadvisor.co.uk/review/mice-tracker-balls/?p=" + str(i)
        time.sleep(1)
        print("Fetching titles from page ", i)
        get_article_titles(url, filename)
        i += 1
    print("Titles are all copied, there should be a file named " + filename\
 + ".txt, in the same folder. Check it out !")


if __name__ == "__main__":
    main()
