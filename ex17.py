# Exercice 17: Decode a webpage

# Print out a list of all the article titles on the newyorktimes website

import time
import random
import requests
from bs4 import BeautifulSoup

bleach = "http://bleacherreport.com/world-football"

def get_article_titles(url):
    new_r = requests.get(url)
    response_html = new_r.text
    soup = BeautifulSoup(response_html, "html.parser")
    article_titles = soup.find_all("span", {"class":"title"})

    for title in article_titles:
        print(title.string)

get_article_titles(bleach)

